from django.urls import path

from . import views

urlpatterns = [
    path('putschedule/', views.putschedule, name='putschedule'),
    path('schedule/', views.thanks, name='thanks'),
    path('delete/<int:id>', views.delete, name='delete')
]