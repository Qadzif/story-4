from django.shortcuts import render
from .forms import ReminderForm
from .models import Reminder
from django.http import HttpResponseRedirect
from django.urls import reverse

targets = {'Schedule' : 'thanks', 'Put Schedule' : 'putschedule'}

# Create your views here.

def putschedule(request):
    if request.method == 'POST':
        jadwal = ReminderForm(request.POST)

        if jadwal.is_valid():
            jadwal.save()

            return HttpResponseRedirect(reverse('thanks'))
        else:
            return render(request, 'putschedule.html',\
                {
                    'navigate' : '',
                    'form' : jadwal,
                    'targets' : {
                        'Schedule' : 'thanks'
                    }
                })

    return render(request, 'putschedule.html',\
        {
            'navigate' : '',
            'form' : ReminderForm,
            'targets' : {
                'Schedule' : 'thanks'
            }
        })

def thanks(request):
    jadwal = Reminder.objects.all()
    return render(request, 'response/thanks.html',
        {
            'schedule_data' : jadwal,
            'targets' : {
                'Put Schedule' : 'putschedule'
            },
        })

def delete(request, id):
    jadwal = Reminder.objects.get(pk=id)
    jadwal.delete()
    return HttpResponseRedirect(reverse('thanks'))