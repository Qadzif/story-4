from django import forms
from .models import Reminder

class ReminderForm(forms.ModelForm):
    class Meta:
        model = Reminder
        fields = '__all__'
        widgets = {\
            'date' : forms.DateInput(attrs={'type':'date'}),
            'time' : forms.TimeInput(attrs={'type':'time'}),
            }
    pass