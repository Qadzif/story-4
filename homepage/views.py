from django.shortcuts import render
from .forms import ReminderForm
from .models import Reminder
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
def index(request):
    return render(request, "index.html")

def gallery(request):
    return render(request, "gallery.html")

def project(request):
    return render(request, "project.html")

def putschedule(request):
    if request.method == 'POST':
        jadwal = ReminderForm(request.POST)

        if jadwal.is_valid():
            jadwal.save()

            return HttpResponseRedirect(reverse('thanks'))
        else:
            return render(request, 'putschedule.html',\
                {
                    'navigate' : '',
                    'form' : jadwal,
                    'targets' : {
                        'Home' : 'homepage:index',
                        'Schedule' : 'homepage:thanks',
                    }
                })

    return render(request, 'putschedule.html',\
        {
            'navigate' : '',
            'form' : ReminderForm,
            'targets' : {
                'Home' : 'homepage:index',
                'Schedule' : 'homepage:thanks',
            }
        })

def thanks(request):
    jadwal = Reminder.objects.all()
    return render(request, 'response/thanks.html',
        {
            'schedule_data' : jadwal,
            'targets' : {
                'Home' : 'homepage:index',
                'Put Schedule' : 'homepage:putschedule',
            },
        })

def delete(request, id):
    jadwal = Reminder.objects.get(pk=id)
    jadwal.delete()
    return HttpResponseRedirect(reverse('thanks'))

