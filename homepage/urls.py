from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('gallery/', views.gallery, name='gallery'),
    path('project/', views.project, name='project'),
    path('putschedule/', views.putschedule, name='putschedule'),
    path('schedule/', views.thanks, name='schedule'),
    path('delete/<int:id>', views.delete, name='delete')
]
