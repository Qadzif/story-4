from django.db import models
from django import forms
from django.utils import timezone

# Create your models here.
class Reminder(models.Model):
    name = models.CharField(max_length=2147483647, default='')
    date = models.DateField()
    time = models.TimeField(help_text='HH:MM AM/PM', default=timezone.now())
    location = models.CharField(max_length=2147483647, default='')
    category = models.CharField(max_length=2147483647, default='')
